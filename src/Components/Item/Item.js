import {useState} from "react";
import './Item.scss';
import PropTypes from 'prop-types';

function Item(props) {
    const [ampliado, setAmpliado] = useState(false);

    const handleClick = () => {
        setAmpliado(!ampliado);
        props.callback(props.data.name);
    }

    return(
        <li
            id={props.id}
            onClick={handleClick}
            className="item"
        >
            <h3>{props.data.name}</h3>:
            {
                ampliado? <h3>{props.data.value}</h3> : null
            }
        </li>
    )
}

export default Item;

Item.propTypes = {
    data: PropTypes.exact({
       name: PropTypes.string,
       value: PropTypes.number,
    }),
    id: PropTypes.string.isRequired,
}

import styles from './Alert.module.scss';

function Alert(props) {

    return(
        <div className={styles.alert}>
            <p className={styles.text}>Error: {props.text}</p>
        </div>
    )
}

export default Alert;
import React from 'react'

class Contador extends React.Component{
    intervalo;

    constructor(props) {
        super(props);
        this.state = {
            cuenta: 0,
        };
    }

    componentDidMount() {
        this.intervalo = setInterval(() => {
            const cuenta = this.state.cuenta + 1;
            this.setState({
                cuenta,
            })
        }, 1000);
    }

    componentWillUnmount() {
        clearInterval(this.intervalo);
    }

    render() {
        return(
            <h2>
                {this.state.cuenta}
            </h2>
        )
    }
}

export default Contador;
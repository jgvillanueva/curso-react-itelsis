import React from 'react'
import List from "../List/List";
import Contador from "../Contador/Contador";
import Alert from "../Alert/Alert";
export class Maincomponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            listaVisible: true,
            elements: [],
            fetchError: false,
            errorText: ''
        }
        this.handleClick = this.handleClick.bind(this);
        this.receiveListError = this.receiveListError.bind(this);
    }

    async componentDidMount() {
        let error = false;
        let errorText = '';
        try {
            await this.getData();
        } catch (e) {
            console.log(e);
            error = true;
            errorText = e.message;
        }

        this.setState({
            fetchError: error,
            errorText: errorText,
        })
    }

    async getData() {
        return new Promise((resolve, reject) => {
            fetch('https://api.frankfurter.app/latest')
                .then(response => response.json())
                .then((response) => {
                    console.log(response);
                    const data = [];
                    Object.keys(response.rates).forEach((key) => {
                        data.push({
                            name: key,
                            value: response.rates[key],
                        });
                    })
                    this.setState({
                        elements: data,
                    })
                    resolve(response);
                })
                .catch((e) => {
                    reject(e);
                })
        })
    }

    handleClick() {
        const listaVisible = this.state.listaVisible;
        this.setState({
            listaVisible: !listaVisible
        })
    }
    
    receiveListError(fetchError, errorText) {
        this.setState({
            fetchError,
            errorText,
        })
    }

    render() {
        let alert;
        if(this.state.fetchError) {
            alert = <Alert text={this.state.errorText} />
        }

        return(
            <div>
                <h1>{this.props.texto}</h1>
                <Contador />
                { alert }
                <button onClick={this.handleClick}>Toogle list</button>
                {
                    this.state.listaVisible ?
                        <List  data={this.state.elements} fetchErrorCallback={this.receiveListError}/>
                        :
                        null
                }

            </div>
        )
    }
}

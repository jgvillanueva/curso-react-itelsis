import logo from './logo.svg';
import './App.css';
import {Maincomponent} from "./Components/MainComponent/Maincomponent";
import React from 'react';

function App() {
/*
    const createElement = () => {
        return React.createElement(
            'h1',
            {
                className: 'title'
            },
            'Hola mundo'
        )
    }
*/
  return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
            { /* createElement() */}
          <Maincomponent texto="HOLA MUNDO" />
        </header>
      </div>
  );
}

export default App;

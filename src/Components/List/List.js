import Item from "../Item/Item";
import {useEffect, useState} from "react";
import './List.css';

function List(props) {
    const [selected, setSelected] = useState('');
    const [cantidad, setCantidad] = useState(0);
    const [cambio, setCambio] = useState();

    const getItemClick = (value) => {
        setSelected(value);
    }
    const drawElements = () => {
        const items = props.data.map((element, i) => {
            return <Item
                data={element}
                key={i}
                id={'li' + i}
                callback={getItemClick}
            ></Item>
        })
        return items;
    }

    const handleInputChange = (event) => {
        setCantidad(event.target.value);
    }

    useEffect(() => {
        const getCambio = () => {
            if (selected === '') return;

            return new Promise(async () => {
                fetch(`https://api.frankfurter.app/latest?amount=${cantidad}&from=EUR&to=${selected}`)
                    .then((response) => response.json())
                    .then((response) => {
                        console.log('Recibido el cambio', response);
                        setCambio(response.rates[selected]);
                        this.props.fetchErrorCallback({
                            fetchError: false,
                            errorText: '',
                        });
                    })
                    /*.catch((e) => {
                        props.fetchErrorCallback({
                            fetchError: true,
                            errorText: e.message,
                        });
                    })*/
            });
        }

        getCambio();
    }, [selected, cantidad])

    return(
        <div className="list">
            <h2>{selected}</h2>
            <div>
                <h2>El cambio es: {cambio}</h2>
                <input onChange={handleInputChange}/>
            </div>
            <ul>
                {
                    drawElements()
                }
            </ul>
        </div>
    )
}

export default List;